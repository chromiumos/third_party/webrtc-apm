# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

load("//:defs.bzl", "COPTS")

package(
    default_visibility = ["//:__subpackages__"],
)

cc_library(
    name = "fft_size_128",
    srcs = ["fft_size_128/ooura_fft.cc"] + select({
        "//:neon_build": ["fft_size_128/ooura_fft_neon.cc"],
        "//conditions:default": [],
    }) + select({
        "@platforms//cpu:x86_64": ["fft_size_128/ooura_fft_sse2.cc"],
        "//conditions:default": [],
    }),
    hdrs = [
        "fft_size_128/ooura_fft.h",
        "fft_size_128/ooura_fft_tables_common.h",
        "fft_size_128/ooura_fft_tables_neon_sse2.h",
    ],
    copts = COPTS,
    deps = [
        "//rtc_base/system:arch",
        "//system_wrappers",
    ] + select({
        "//:neon_build": ["//common_audio"],
        "//conditions:default": [],
    }),
)

cc_library(
    name = "fft_size_256",
    srcs = ["fft_size_256/fft4g.cc"],
    hdrs = ["fft_size_256/fft4g.h"],
    copts = COPTS,
)
